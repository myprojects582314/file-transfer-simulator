﻿// Autor: Piotr Hajduk s189459 
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#pragma pack(1)

struct InfoAboutPackage {
	char package_name;
	int number_of_package;
};

int main() {

	int dataSizeInPackage = 10;
	int frameSize = sizeof(InfoAboutPackage);
	//std::string fileName = "testTXT.txt";
	std::string fileName = "testPDF.pdf";
	//std::string fileName = "test.jpg";
	int fileSize;

	std::ifstream inputFile{fileName, std::ios::binary};
	inputFile.seekg(0, std::ios::end);
	fileSize = (int)inputFile.tellg();
	inputFile.clear();
	inputFile.seekg(0); // Set pointer to begining of file

	std::cout << "Size of package's frame: " << frameSize << std::endl;
	std::cout << "Size of data in package: " << dataSizeInPackage << std::endl;
	std::cout << "Loaded file name: " << fileName << std::endl;
	std::cout << "Size of loaded file: " << fileSize << " Bytes" << std::endl;

	std::vector<char*> pointerToPackage;
	int numberOfPackage = fileSize / dataSizeInPackage;
	if (fileSize % dataSizeInPackage != 0) { numberOfPackage++; }


	auto start = std::chrono::high_resolution_clock::now();

	try 
	{
		for (int i = 0; i < numberOfPackage + 1; ++i) {
			char* buffer = (char*)malloc(frameSize + dataSizeInPackage);
			pointerToPackage.push_back(buffer);
		}
	}
	catch (std::exception e) 
	{
		std::cerr << e.what() << std::endl;
	}

	try 
	{
		for (int i = 0; i < numberOfPackage - 1; ++i) {
			InfoAboutPackage* packageInfo = (InfoAboutPackage*)(pointerToPackage[i]);
			packageInfo->number_of_package = i;
			packageInfo->package_name = (char)i;
			inputFile.read(pointerToPackage[i] + frameSize, dataSizeInPackage);
		}
		InfoAboutPackage* packageInfo = (InfoAboutPackage*)(pointerToPackage[numberOfPackage]);
		packageInfo->number_of_package = numberOfPackage;
		packageInfo->package_name = (char)numberOfPackage;
		inputFile.read(pointerToPackage[numberOfPackage] + frameSize, fileSize - (numberOfPackage - 1)*dataSizeInPackage);
		inputFile.close();
	}
	catch (std::exception e) 
	{
		std::cerr << e.what() << std::endl;
	}


	std::string prefixFile = "out_";
	std::string outFileName = prefixFile + fileName;
	std::ofstream outputFile{outFileName, std::ios::binary | std::ios::out};


	try 
	{
		for (int i = 0; i < numberOfPackage - 1; ++i) {
			outputFile.write(pointerToPackage[i] + frameSize, dataSizeInPackage);
		}
		outputFile.write(pointerToPackage[numberOfPackage] + frameSize, fileSize - (numberOfPackage - 1) * dataSizeInPackage);
		outputFile.close();
	}
	catch (std::exception e) 
	{
		std::cerr << e.what() << std::endl;
	}

	auto end = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	std::cout << "Duration of copyin file weight: " << fileSize << " B" << " is: " << duration.count() << " microseconds" << std::endl;
	return 0;
}