# File transfer simulator
> program read any file divide it into package and copy it to another file

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Project Status](#project-status)
* [Contact](#contact)


## General Information
- This project was realized on V semester of studies.
- Program C++ language to send any file (copy) to another place
- This is only educational project to understand how data transfwer works


## Technologies Used
- C++ version 17
- Visual Studio 2022


## Features
Provided features:
- Read a file as bytes
- Read from file size
- Calculate how many package will be need
- Reserve memory for package with frame (name and number)
- Read into this memory some data
- Recive and write to new file this data


## Project Status
Project is: _no longer being worked on_. 


## Contact
Created by Piotr Hajduk

